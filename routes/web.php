<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('flash', function () {
    return view('flash');
});

Route::get('html', function () {
    return view('html');
});

Route::get('embed', function () {
    return view('embed');
});