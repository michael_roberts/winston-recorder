@extends('layout')

@section('content')
    <h1>HTML recorder demo</h1>
    <p>Based on getUserMedia and is only compatible on certain platforms</p>
    <h3>TODO</h3>
    <ul>
        <li>Add constraints</li>
        <li>Add input fields on page to update in realtime</li>
    </ul>
    <video></video>
    <div>
        <button onclick="start()">Start</button>
        <button onclick="stop()">Stop</button>
    </div>
    <div>
        download this?
        <ul id="ul"></ul>
    </div>

@endsection

@section('footer')

<script type="text/javascript">

//        var getUserMedia = (navigator.getUserMedia ||
//        navigator.webkitGetUserMedia ||
//        navigator.mozGetUserMedia);
//
//        // Some browsers just don't implement it - return a rejected promise with an error
//        // to keep a consistent interface
//        if(!getUserMedia) {
//            return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
//        }
//
//        var p = navigator.mediaDevices.getUserMedia({
//            audio: true,
//            video: {
//                width: { min: 1280 },
//                height: { min: 720 },
//                frameRate: {
//                    ideal: 25,
//                    max: 30
//                }
//            }
//        });
//
//        p.then(function(mediaStream) {
//            var video = document.querySelector('video');
//            video.src = window.URL.createObjectURL(mediaStream);
//            video.onloadedmetadata = function(e) {
//                video.play();
//            };
//        });
//
//        p.catch(function(err) {
//            console.log(err.name);
//        });

    var promisifiedOldGUM = function(constraints) {

        // First get ahold of getUserMedia, if present
        var getUserMedia = (navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia);

        // Some browsers just don't implement it - return a rejected promise with an error
        // to keep a consistent interface
        if( ! getUserMedia) {
            return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
        }

        // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
        return new Promise(function(resolve, reject) {
            getUserMedia.call(navigator, constraints, resolve, reject);
        });

    };

    // Older browsers might not implement mediaDevices at all, so we set an empty object first
    if(navigator.mediaDevices === undefined) {
        navigator.mediaDevices = {};
    }

    // Some browsers partially implement mediaDevices. We can't just assign an object
    // with getUserMedia as it would overwrite existing properties.
    // Here, we will just add the getUserMedia property if it's missing.
    if(navigator.mediaDevices.getUserMedia === undefined) {
        navigator.mediaDevices.getUserMedia = promisifiedOldGUM;
    }


    // Prefer camera resolution nearest to 1280x720.
    var constraints = {
        audio: true,
        video: true
//        video: {
//            width: 1280,
//            height: 720
//        }
    };

    var file;
    var videoStream;
    var player = document.querySelector('video');
    var recorder;
    var ul = document.getElementById('ul');
    var chunks = [];
    var url = window.URL || window.webkitURL;


    navigator.mediaDevices.getUserMedia(constraints)
        .then(function(stream) {

            recorder = new MediaRecorder(stream, {
                mimeType: 'video/webm'
            });

            recorder.start(10);

            player.src = url ? url.createObjectURL(stream) : stream;
            player.play();


            recorder.onerror = function(e){
                log('Error: ' + e);
                console.log('Error: ', e);
            };

            recorder.ondataavailable = function(e) {
                chunks.push(e.data);
            };

            recorder.onstop = function (e) {
                var blob = new Blob(chunks, { 'type' : 'video/webm' });

                console.log('chunks', chunks);

                var a  = document.createElement('a'),
                        li = document.createElement('li');

                var videoRecording = url.createObjectURL(blob);

                player.src = videoRecording;
                player.controls = true;
                player.play();


                a.download = ['video-', (new Date() + '').slice(4, 28), '.webm'].join('');
                a.href = videoRecording;
                a.textContent = a.download;
                li.appendChild(a);
                ul.appendChild(li);
            };

        })
        .catch(function(err) {
            console.log(err.name + ": " + err.message);
        });

    var stop = function() {

        recorder.stop();
    };


</script>

@endsection