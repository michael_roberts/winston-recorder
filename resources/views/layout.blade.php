<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Winston</title>
    @yield('head')
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <a href="/">Home</a>
        @include('_links')
        @yield('content')
    </div>
</div>
@yield('footer')
</body>
</html>
